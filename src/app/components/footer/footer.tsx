import React from 'react';

interface propsFooter {}

const Footer: React.FC<propsFooter> = () => {
    return (
        <footer className='bg-white h-12 fixed bottom-0 w-full shadow-inner justify-center items-center'>
            <h2 className='pt-3 text-center text-gray-600'>Copyright 2024 © TangDev</h2>
        </footer>
    )
}

export default Footer
