'use client'
import React, { useState } from 'react'
import Image from 'next/image'
import './card.scss'
import { generateThaiIdCard, copyThaiIdCard } from './cardHepler'

interface propsCard {}

const Card: React.FC<propsCard> = () => {
    const [thaiIdCardNumber, setThaiIdCardNumber] = useState<string | null>(null)
          
    const handleGenerateClick = () => {
        const generatedId = generateThaiIdCard()
        setThaiIdCardNumber(generatedId)
    }

    const handleCopyClick = () => {
        copyThaiIdCard(thaiIdCardNumber)
    }

    return (
        <div className='mx-auto mt-40 mb-40 max-w-screen-md justify-center'>
            <div className='shadow-2xl rounded-xl bg-white text-black'>
                <div className='p-10'>
                    <h1 className='text-gray-600 text-center font-semibold text-xl'>Thai National ID Card Generator</h1>
                </div>
                <div className='flex items-center'>
                    <div className='mx-auto w-[21rem]'>
                        <div className='shadow-lg rounded-xl bg-sky-300'>
                            <div className='h-[13rem]'>
                                <div className='flex items-center pt-3'>
                                    <div className='ml-2 rounded-full bg-gray-200 w-12 h-12'></div>
                                    <div className='flex flex-col ml-3'>
                                        <div className='flex items-center'>
                                            <h2 className='text-xs text-gray-100'>บัตรประจำตัวประชาชน</h2>
                                            <h2 className='text-xs text-blue-500 pl-1'>Thai National ID Card</h2>
                                        </div>
                                        <div className='flex flex-col ml-3'>
                                            <h1 className='mt-2 text-center text-gray-600 text-md font-semibold'>{ thaiIdCardNumber ?? '_ _ _ _ _ _ _ _ _ _ _ _ _' }</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className='flex'>
                                    <div className='bg-gray-200 w-5 h-[8.30rem] mt-2 ml-3 rounded'></div>
                                    <div className='bg-yellow-400 w-12 h-10 mt-8 ml-2 rounded-lg'></div>
                                    <div className='w-[5rem] ml-36'>
                                        <div className='mt-12 shadow-md rounded-xl bg-gray-200 h-[5rem]'>
                                            <Image className='img w-70 pt-2 pl-2' src='/assets/logos/hacker.png' width={70} height={70} alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='flex items-center justify-center space-x-4 mt-10'>
                    <button className='bg-sky-500 w-48 h-[5rem] rounded-lg mb-16 hover:bg-sky-700 transition duration-300' onClick={handleGenerateClick}>
                        <h2 className='text-gray-100 font-semibold'>Generate</h2>
                    </button>
                    <button className='bg-teal-400 w-48 h-[5rem] rounded-lg mb-16 hover:bg-teal-600 transition duration-300' onClick={handleCopyClick}>
                        <h2 className='text-gray-100 font-semibold'>Copy</h2>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Card
