export function generateThaiIdCard(): string {
    let thaiIDCardNumber

    do {
        let randomDigits = ""
        for (let i = 0; i < 12; i++) {
            randomDigits += Math.floor(Math.random() * 10)
        }

        let sum = 0;
        for (let i = 0; i < 12; i++) {
            sum += parseInt(randomDigits[i]) * (13 - i)
        }

        const remainder = sum % 11
        const checkDigit = (11 - remainder) % 10

        thaiIDCardNumber = `${randomDigits}${checkDigit}`
        thaiIDCardNumber = thaiIDCardNumber.slice(0, 13)
    } while (thaiIDCardNumber.length !== 13)

    return thaiIDCardNumber
}

export function copyThaiIdCard(thaiIDCardNumber: string | null) {
    const textToCopy = thaiIDCardNumber ?? ''

    if (navigator.clipboard) {
        navigator.clipboard.writeText(textToCopy)
    }
}
