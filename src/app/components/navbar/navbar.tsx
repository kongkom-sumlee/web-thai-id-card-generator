import React from 'react'
import Image from 'next/image'

const navbar: React.FC<any> = () => {
    return (
        <nav className='bg-white shadow-lg'>
            <div className='flex pl-4 items-center'>
                <Image className='pt-1 pb-1' src='/assets/logos/id-card.png' width={35} height={35} alt='' />
                <h4 className='pl-2 text-[16px] font-medium text-gray-600 align-middle'>Thai National ID Card Generator</h4>
            </div>
        </nav>
    )
}

export default navbar
