import Navbar from '@/app/components/navbar/navbar'
import Card from '@/app/components/card/card'
import Footer from '@/app/components/footer/footer'

export default function Home() {
  return (
    <div>
      <Navbar />
      <Card />
      <Footer />
    </div>
  )
}
